<?php

//var_dump($_POST);
if(count($_POST) == 0){

}else{

    $rd_gallery_config_options = array(
        'rd_gallery_num_colum'          => $_POST['rd_gallery_num_colum'],
        'rd_gallery_height_colum'       => $_POST['rd_gallery_height_colum'],
        'rd_gallery_show_title'         => $_POST['rd_gallery_show_title'],
        'rd_gallery_show_info_image'    => $_POST['rd_gallery_show_info_image'],
        'rd_gallery_color_hover_image'  => $_POST['rd_gallery_color_hover_image'],
        'rd-gallery-effect-hover'       => $_POST['rd-gallery-effect-hover'],
        'rd_gallery_lightbox'       => $_POST['rd_gallery_lightbox']
    );
    $result_data = update_option( 'rd_gallery_option', $rd_gallery_config_options);
}

// Options
$options = get_option('rd_gallery_option');

?>

<div>

    <h2><?php echo WP_RDGALLERY_NAME_PLUGINS; ?></h2>
    <?php

        if($result_data == true){
            echo "<div class='updated settings-error notice is-dismissible'> 
                        <p><strong>Configuración Guardada.</strong></p>
                    </div>";
        }

    ?>

    <hr>

    <form method="POST" action="#">
        <h3>Opciones de Galerias</h3>
        <p>Opciones configurables y parametros de Galerias.</p>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row">
                    <label for="my-text-field">Numero de Columnas:</label>
                </th>
                <td>
                    <input type="number" value="<?= $options['rd_gallery_num_colum'] ?>" id="rd-gallery-num-colum" name="rd_gallery_num_colum" placeholder="2-4" min="2" max="4">
                    <span class="description">Numero de Columnas Verticales que tendrá cada Galeria de manera General.</span>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="my-text-field">Altura de Columnas:</label>
                </th>
                <td>
                    <select id="rd-gallery-height-colum" name="rd_gallery_height_colum">
                        <option value="auto" <?php if($options['rd_gallery_height_colum'] == "auto"){echo "selected";} ?>>(Auto) - Defecto</option>
                        <option value="300" <?php if($options['rd_gallery_height_colum'] == "300"){echo "selected";} ?>>300px</option>
                        <option value="400" <?php if($options['rd_gallery_height_colum'] == "400"){echo "selected";} ?>>400px</option>
                        <option value="500" <?php if($options['rd_gallery_height_colum'] == "500"){echo "selected";} ?>>500px</option>
                        <option value="600" <?php if($options['rd_gallery_height_colum'] == "600"){echo "selected";} ?>>600px</option>
                    </select>
                    <span class="description">Altura que tendrá cada item de la Galeria de manera General.</span>
                </td>
            </tr>
            <tr>
                <th scope="row">Opciones de Imagenes</th>
                <td>
                    <fieldset>
                        <label for="comments_notify">
                            <input type="checkbox" value="1" id="rd-gallery-show-title" name="rd_gallery_show_title" <?php if($options['rd_gallery_show_title'] == 1){echo "checked";} ?>> Título de Imágenes
                        </label>
                        <br>
                        <label for="moderation_notify">
                            <input type="checkbox" value="1" id="rd-gallery-show-info-image" name="rd_gallery_show_info_image" <?php if($options['rd_gallery_show_info_image'] == 1){echo "checked";} ?>> Información de Imágenes
                        </label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row">Color de Fondo de Imagen</th>
                <td>
                    <fieldset>
                        <label for="comments_notify">
                            <input type="text" class="color-picker" data-alpha="true" data-default-color="0,0,0,0.85" name="rd_gallery_color_hover_image" value="<?= $options['rd_gallery_color_hover_image'] ?>"/>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row">
                    <label for="start_of_week">Efecto de Hover de Imagenes</label>
                </th>
                <td>
                    <select id="rd-gallery-effect-hover" name="rd-gallery-effect-hover">
                        <option value="lily" <?php if($options['rd-gallery-effect-hover'] == "lily"){echo "selected";} ?>>(lily) - Defecto</option>
                        <option value="sadie" <?php if($options['rd-gallery-effect-hover'] == "sadie"){echo "selected";} ?>>Efecto Sadie</option>
                        <option value="roxy" <?php if($options['rd-gallery-effect-hover'] == "roxy"){echo "selected";} ?>>Efecto Roxy</option>
                        <option value="bubba" <?php if($options['rd-gallery-effect-hover'] == "bubba"){echo "selected";} ?>>Efecto Bubba</option>
                        <option value="romeo" <?php if($options['rd-gallery-effect-hover'] == "romeo"){echo "selected";} ?>>Efecto Romeo</option>
                        <option value="layla" <?php if($options['rd-gallery-effect-hover'] == "layla"){echo "selected";} ?>>Efecto Layla</option>
                        <option value="honey" <?php if($options['rd-gallery-effect-hover'] == "honey"){echo "selected";} ?>>Efecto Honey</option>
                        <option value="oscar" <?php if($options['rd-gallery-effect-hover'] == "oscar"){echo "selected";} ?>>Efecto Oscar</option>
                        <option value="marley" <?php if($options['rd-gallery-effect-hover'] == "marley"){echo "selected";} ?>>Efecto Marley</option>
                        <option value="ruby" <?php if($options['rd-gallery-effect-hover'] == "ruby"){echo "selected";} ?>>Efecto Ruby</option>
                        <option value="milo" <?php if($options['rd-gallery-effect-hover'] == "milo"){echo "selected";} ?>>Efecto Milo</option>
                        <option value="dexter" <?php if($options['rd-gallery-effect-hover'] == "dexter"){echo "selected";} ?>>Efecto Dexter</option>
                        <option value="sarah" <?php if($options['rd-gallery-effect-hover'] == "sarah"){echo "selected";} ?>>Efecto Sarah</option>
                        <option value="zoe" <?php if($options['rd-gallery-effect-hover'] == "zoe"){echo "selected";} ?>>Efecto Zoe (Malo)</option>
                        <option value="chico" <?php if($options['rd-gallery-effect-hover'] == "chico"){echo "selected";} ?>>Efecto Chico</option>
                    </select>
                </td>
            </tr>
            </tbody>
        </table>
        <hr>
        <h3>Opciones de LightBox</h3>
        <p>Opciones configurables del lightboxt de las Galerias.</p>
        <table class="form-table">
            <tbody>
            <tr>
                <th scope="row">
                    <label for="my-text-field">LightBox:</label>
                </th>
                <td>
                    <input type="radio" value="1" id="rd-gallery-lightbox" name="rd_gallery_lightbox" <?php if($options['rd_gallery_lightbox'] == 1){echo "checked";} ?>>Si &nbsp;
                    <input type="radio" value="0" id="rd-gallery-lightbox" name="rd_gallery_lightbox" <?php if($options['rd_gallery_lightbox'] == 0){echo "checked";} ?>>No
                </td>
            </tr>
            </tbody>
        </table>

        <?php  submit_button(); ?>
    </form>

</div>

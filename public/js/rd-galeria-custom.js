

jQuery(document).ready(function($) {


    // Custom RD Galeria
    $(".rd_galeria").each(function() {

        let magicGrid = new MagicGrid({
            container: '.rd_galeria',
            static: true, // Required for static content. Default: false.
            items: 2, // Required for dynamic content. Initial number of items in the container.
            gutter: 10, // Optional. Space between items. Default: 25(px).
            maxColumns: 5, // Optional. Maximum number of columns. Default: Infinite.
            useMin: true, // Optional. Prioritize shorter columns when positioning items. Default: false.
            useTransform: true, // Optional. Position items using CSS transform. Default: True.
            animate: true, // Optional. Animate item positioning. Default: false.
        });

        magicGrid.listen();



        // init Masonry
        /*var $grid = $('.rd_galeria').masonry({
            itemSelector: '.rd_galeria_item',
            columnWidth: '.rd_galeria_item_size',
            percentPosition: true,
            gutter: 10,
            fitWidth: true,
            horizontalOrder: true
        });

        // layout Masonry after each image loads
        $grid.imagesLoaded().progress( function() {
            $grid.masonry();
        });*/

        $('[data-fancybox="gallery"]').fancybox({
            closeExisting: false,
            loop: false, // Enable infinite gallery navigation
            preventCaptionOverlap: true, // Should allow caption to overlap the content
            infobar: true, // Should display counter at the top left corner
            // What buttons should appear in the top right corner.
            // Buttons will be created using templates from `btnTpl` option
            // and they will be placed into toolbar (class="fancybox-toolbar"` element)
            buttons: [
                //"zoom",
                //"share",
                //"slideShow",
                //"fullScreen",
                //"download",
                //"thumbs",
                "close"
            ],
            protect: false, // Disable right-click and use simple image protection for images
            modal: false, // Shortcut to make content "modal" - disable keyboard navigtion, hide buttons, etc
            arrows: true, // Should display navigation arrows at the screen edges
            keyboard: true, // Enable keyboard navigation
            defaultType: "image", // Default content type if cannot be detected automatically
            // Open/close animation type
            // Possible values:
            //   false            - disable
            //   "zoom"           - zoom images from/to thumbnail
            //   "fade"
            //   "zoom-in-out"
            //
            animationEffect: "zoom",
            animationDuration: 366, // Duration in ms for open/close animation
            // Transition effect between slides
            // Possible values:
            //   false            - disable
            //   "fade'
            //   "slide'
            //   "circular'
            //   "tube'
            //   "zoom-in-out'
            //   "rotate'
            //
            transitionEffect: "slide",
            transitionDuration: 366, // Duration in ms for transition animation
            lang: "es",
            i18n: {
                en: {
                    CLOSE: "Close",
                    NEXT: "Next",
                    PREV: "Previous",
                    ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
                    PLAY_START: "Start slideshow",
                    PLAY_STOP: "Pause slideshow",
                    FULL_SCREEN: "Full screen",
                    THUMBS: "Thumbnails",
                    DOWNLOAD: "Download",
                    SHARE: "Share",
                    ZOOM: "Zoom"
                },
                es: {
                    CLOSE: "Cerrar",
                    NEXT: "Siguiente",
                    PREV: "Anterior",
                    ERROR: "El contenido solicitado no se puede cargar. <br/> Por favor intente de nuevo más tarde.",
                    PLAY_START: "Inicar",
                    PLAY_STOP: "Detener",
                    FULL_SCREEN: "Pantalla Completa",
                    THUMBS: "Miniaturas",
                    DOWNLOAD: "Descargar",
                    SHARE: "Compartir",
                    ZOOM: "Acercar"
                }
            }
        });

    });


});

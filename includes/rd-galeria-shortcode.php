<?php


// RD Galeria Shortcode
function rd_gallery_images($atts){

    $a = shortcode_atts(array(
        'titulo' => 'Titulo de Galeria.',
        'id' => '',
        'status' => '1',
        'filas' => '3',
        'titulo-img' => '1',
        'info' => '1',
        'color' => '#ffffff'
    ), $atts);

    $id_rd_gallery = esc_attr($a['id']);
    $title_rd_gallery = esc_attr($a['titulo']);

    $term = get_term($id_rd_gallery);

    // Parametros de Galeria
    $parametros = get_option('rd_gallery_option');
    //var_dump($parametros);
    $param_num_colum = $parametros['rd_gallery_num_colum'];
    $param_height_colum = $parametros['rd_gallery_height_colum'];
    $param_show_title = $parametros['rd_gallery_show_title'];
    $param_show_info_image = $parametros['rd_gallery_show_info_image'];
    $param_background_color_image = $parametros['rd_gallery_color_hover_image'];
    $param_effect_hover = $parametros['rd-gallery-effect-hover'];
    $param_lightbox = $parametros['rd_gallery_lightbox'];

    $args = array(
        'post_type' => 'rd_gallery',
        'post_status' => 'publish',
        'order' => 'ASC',
        'orderby' => 'date',
        'tax_query' => array(
            array(
                'taxonomy' => $term->taxonomy,  // taxonomy name
                'field' => $term->slug,         // term_id, slug or name
                'terms' => $term->term_id       // term id, term slug or term name
            )
        )
    );

    $array_source = new WP_Query($args);
    //var_dump($array_source);



    $content = "<!-- RD Galeria -->";
    $content .= "<div class='rd-galleria-continaer' id='rd-galleria-" . $id_rd_gallery . "'>";
    $content .= "<h1 class='rd-glaria-title '>" . $title_rd_gallery . "</h1>";
    $content .= "<div class='rd_galeria default_styles'>";

    $count = 0;
    foreach ($array_source->posts as $rd_gallery_array) {
        $rd_gallery_id_img = $rd_gallery_array->ID;
        $rd_gallery_title_img = $rd_gallery_array->post_title;
        $rd_gallery_info_img = $rd_gallery_array->post_content;
        $rd_gallery_thumbnail_img_url = wp_get_attachment_image_src(get_post_thumbnail_id($rd_gallery_id_img), 'full');
        $rd_gallery_img = $rd_gallery_thumbnail_img_url[0];
        //add_filter('the_content', $rd_gallery_info_img);

        // Sentencias
        if($param_show_title == 1){ // Titulo
            $rd_gallery_title = '<h2>' . $rd_gallery_title_img . '</h2>';
        }
        if($param_show_info_image == 1){ // Descripcion
            $rd_gallery_info = '<p>' . wp_trim_words($rd_gallery_info_img, 10) . '</p>';
        }
        if($param_lightbox == 1){
            $lighboxt = 'data-fancybox="gallery" href="' . $rd_gallery_img . '" data-caption="' . $rd_gallery_title_img . '"';
        }
        if($param_num_colum == ""){ // Numero de Columnas
            $num_item = "3";
        }else{
            $num_item = $param_num_colum;
        }
        if($param_height_colum == ""){ // Numero de Columnas
            $height_item = "auto";
        }else{
            $height_item = $param_height_colum;
        }

        $content .= '<figure class="rd_galeria_item w_item_' . $num_item . ' h_item_' . $height_item . ' h_item_ effect-' . $param_effect_hover . '" ' . $lighboxt . '>
                        <img class="" src="' . $rd_gallery_img . '" alt="' . $rd_gallery_title_img . '" title="' . $rd_gallery_title_img . '" item="' . $count . '"/>
                        <figcaption class="rd_galeria_info" >
                            <div>
                                ' . $rd_gallery_title . '
                                ' . $rd_gallery_info . '
                                <a href="#">Ver Mas</a>
                            </div>
                        </figcaption>
                    </figure>';


        $count++;
    }
    wp_reset_postdata();

    $content .= '</div>';
    $content .= '</div>';

    $content .= '';

    return $content;

}
add_shortcode('RD-Galeria', 'rd_gallery_images');


?>
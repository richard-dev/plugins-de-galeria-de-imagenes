<?php


/* Disable parent Taxonomy */
add_action( 'admin_head-edit-tags.php', 'wpse_58799_remove_parent_category' );
function wpse_58799_remove_parent_category()
{
    // don't run in the Tags screen
    if ( 'rd_gallery_category' != $_GET['taxonomy'] )
        return;

    // Screenshot_1 = New Category
    // http://example.com/wp-admin/edit-tags.php?taxonomy=category
    $parent = 'parent()';

    // Screenshot_2 = Edit Category
    // http://example.com/wp-admin/edit-tags.php?action=edit&taxonomy=category&tag_ID=17&post_type=post
    if ( isset( $_GET['action'] ) )
        $parent = 'parent().parent()';

    ?>
    <script type="text/javascript">
        jQuery(document).ready(function($)
        {
            $('label[for=parent]').<?php echo $parent; ?>.remove();
        });
    </script>
    <?php
}

/*
 * filter pattern: manage_edit-{taxonomy}_columns
 * where {taxonomy} is the name of taxonomy e.g; 'carousel_category'
 * codex ref: https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_$taxonomy_id_columns
 */
add_filter('manage_edit-rd_gallery_category_columns', 'rd_gallery_category_columns');
function rd_gallery_category_columns($columns)
{
    // remove slug column
    unset($columns['slug']);
    unset($columns['description']);

    // Edit name colum
    $columns['posts'] = __('Imagenes');

    // add carousel shortcode column
    $columns['gallery_shortcode'] = __('Shortcode');
    //$columns['options'] = __('Opciones');
    return $columns;
}

/*
 * filter pattern: manage_{taxonomy}_custom_column
 * where {taxonomy} is the name of taxonomy e.g; 'carousel_category'
 * codex ref: https://codex.wordpress.org/Plugin_API/Filter_Reference/manage_$taxonomy_id_columns
 */
add_filter('manage_rd_gallery_category_custom_column', 'rd_galeria_custom_taxonomy_columns_content', 10, 3);
function rd_galeria_custom_taxonomy_columns_content($content, $column_name, $term_id)
{
    // get the term object
    $term = get_term($term_id, 'rd_gallery_category');

    // check if column is our custom column 'carousel_shortcode'
    if ('gallery_shortcode' == $column_name) {
        $shortcode = '<code>[RD-Galeria titulo="' . $term->name . '" id="' . $term->term_taxonomy_id . '"]</code>';
        $content = $shortcode;
    }

    return $content;
}

//Example from Codex page : http://codex.wordpress.org/Function_Reference/add_submenu_page
//Add this in your functions.php file, or use it in your plugin
add_action('admin_menu', 'register_my_custom_submenu_page');
function register_my_custom_submenu_page() {
    add_submenu_page('edit.php?post_type=rd_gallery',
    __('Opciones'),
    __('Opciones'),
    'manage_options',
    '',
    'rd_gallery_page_options');
}
function rd_gallery_page_options(){
    if (is_admin()) {
        include_once (WP_RDGALLERY_DIR . "/admin/rd_galeria_options_admin.php");
    }
}


/*add_action( 'admin_enqueue_scripts', 'mw_enqueue_color_picker' );
function mw_enqueue_color_picker( $hook_suffix ) {
    // first check that $hook_suffix is appropriate for your admin page
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'my-script-handle', WP_RDGALLERY_URL . 'admin/js/custom.js', array( 'wp-color-picker' ), false, true );
}*/

function mw_enqueue_color_picker($hook) {
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker-alpha',  WP_RDGALLERY_URL . 'admin/js/wp-color-picker-alpha.js', array( 'wp-color-picker' ), '2.1.3', true );
    wp_enqueue_script( 'custom',  WP_RDGALLERY_URL . 'admin/js/custom.js', array( 'wp-color-picker' ), '1.0.0', true );
}
add_action( 'admin_enqueue_scripts',  'mw_enqueue_color_picker' );


// Options configuration RD-Gallery
function rd_gallery_register_settings() {
    add_option( 'rd_gallery_option', '');
    register_setting( 'rd_gallery_option_group', 'rd_gallery_option', '' );
}
add_action( 'admin_init', 'rd_gallery_register_settings' );

// Styles Files (CSS & JS) plugins
add_action('wp_enqueue_scripts', 'rd_gallery_register_css_js');
function rd_gallery_register_css_js() {
    wp_register_style( 'rd-galeria', WP_RDGALLERY_URL . 'public/css/rd-galeria-style.css','',WP_RDGALLERY_VERSION );
    wp_register_style( 'rd-galeria-fancybox', WP_RDGALLERY_URL . 'public/css/jquery.fancybox.min.css','','3.5.6' );
    wp_enqueue_style( 'rd-galeria' );
    wp_enqueue_style( 'rd-galeria-fancybox' );
    //wp_enqueue_script( 'rd-galery', WP_RDGALLERY_URL . 'public/js/jquery.min.js', array( 'jquery' ), '2.1.3' );
    wp_enqueue_script( 'rd-galery', WP_RDGALLERY_URL . 'public/js/jquery.3.3.1.min.js', array( 'jquery' ), '3.3.1' );
    wp_enqueue_script( 'rd-galery-masgic-grid', WP_RDGALLERY_URL . 'public/js/magic-grid.min.js', array( 'jquery' ) );
    //wp_enqueue_script( 'rd-galery-masonry', WP_RDGALLERY_URL . 'public/js/masonry.pkgd.min.js', array( 'jquery' ) );
    //wp_enqueue_script( 'rd-galery-imagesloaded', WP_RDGALLERY_URL . 'public/js/imagesloaded.pkgd.min.js', array( 'jquery' ) );
    wp_enqueue_script( 'rd-galery-fancybox', WP_RDGALLERY_URL . 'public/js/jquery.fancybox.min.js', array( 'jquery' ),'3.5.6' );
    wp_enqueue_script( 'rd-galery-custom', WP_RDGALLERY_URL . 'public/js/rd-galeria-custom.js', array( 'jquery' ), WP_RDGALLERY_VERSION, true );
}

?>
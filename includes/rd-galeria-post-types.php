<?php
/**
 * Register Post type functionality
 */

/**
 * Function to register post type
 */
function rd_gallery_register_post_type() {

    $rdgallery_post_lbls = apply_filters( 'rd_gallery_post_labels', array(
        'name'                 	=> __('RD Galerías', 'rd-gallery'),
        'singular_name'        	=> __('RD Galerías', 'rd-gallery'),
        'add_new'              	=> __('Agregar Imagen', 'rd-gallery'),
        'add_new_item'         	=> __('Agregar Nueva Imagen', 'rd-gallery'),
        'edit_item'            	=> __('Editar Imagen', 'rd-gallery'),
        'new_item'             	=> __('Nueva Imagen', 'rd-gallery'),
        'view_item'            	=> __('Ver Imagen', 'rd-gallery'),
        'search_items'         	=> __('Buscar Imagen', 'rd-gallery'),
        'not_found'            	=> __('No hay Imagenes', 'rd-gallery'),
        'not_found_in_trash'   	=> __('No hay en la Papelera', 'rd-gallery'),
        'parent_item_colon'    	=> '',
        'menu_name'            	=> __('RD Galerías', 'rd-gallery'),
        'featured_image'		=> __('Imagen', 'rd-gallery'),
        'set_featured_image'	=> __('Cargar Imagen', 'rd-gallery'),
        'remove_featured_image'	=> __('Quitar Imagen', 'rd-gallery'),
        'use_featured_image'	=> __('Usar esta Imagen', 'rd-gallery'),
    ));

    $rdgallery_args = array(
        'labels'				=> $rdgallery_post_lbls,
        'public'              	=> false,
        'show_ui'             	=> true,
        'query_var'           	=> false,
        'rewrite'             	=> false,
        'capability_type'     	=> 'post',
        'hierarchical'        	=> false,
        'menu_icon'				=> 'dashicons-format-image',
        'supports'            	=> apply_filters('rd_gallery_post_supports', array('title','thumbnail', 'editor')),
    );

    // Register rd_gallery-owl-carousel post type
    register_post_type( WP_RDGALLERY_POST_TYPE, apply_filters( 'rd_gallery_registered_post_type_args', $rdgallery_args ) );
}

// Action to register plugin post type
add_action('init', 'rd_gallery_register_post_type');

/**
 * Function to register taxonomy
 */
function rd_gallery_register_taxonomies() {

    $rdgallery_cat_lbls = apply_filters('rd_gallery_cat_labels', array(
        'name'              => __( 'Galerías', 'rd-gallery' ),
        'singular_name'     => __( 'Galería', 'rd-gallery' ),
        'search_items'      => __( 'Buscar Galería', 'rd-gallery' ),
        'all_items'         => __( 'Todas las Galerías', 'rd-gallery' ),
        'parent_item'       => __( 'Padre Galería', 'rd-gallery' ),
        'parent_item_colon' => __( 'Padre Galería:', 'rd-gallery' ),
        'edit_item'         => __( 'Editar Galería', 'rd-gallery' ),
        'update_item'       => __( 'Actualizar Galería', 'rd-gallery' ),
        'add_new_item'      => __( 'Agregar Nueva Galería', 'rd-gallery' ),
        'new_item_name'     => __( 'Nombre de Galería', 'rd-gallery' ),
        'menu_name'         => __( 'Galerías', 'rd-gallery' ),
    ));

    $rdgallery_cat_args = array(
        'public'			=> false,
        'hierarchical'      => true, // Parent Taxonomy
        'labels'            => $rdgallery_cat_lbls,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => false,
    );

    // Register rd_gallery-owl-carousel category
    register_taxonomy( WP_RDGALLERY_CAT, array( WP_RDGALLERY_POST_TYPE ), apply_filters('rd_gallery_registered_cat_args', $rdgallery_cat_args) );
}

// Action to register plugin taxonomies
add_action( 'init', 'rd_gallery_register_taxonomies');




?>
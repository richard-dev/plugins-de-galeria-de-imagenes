<?php
/*
Plugin Name: RD Galerías
Plugin URI: https;//rd-galeria.richard-dev.com
Description: Plugin WordPress para crear fantasticas galerias de imagenes con efectos hover y LightBox.
Version: 1.0
Author: Richard Bolivar
Author URI: https://richard-dev.com
License: GPL2
*/

/* Definicion de Constantes */
if( !defined( 'WP_RDGALLERY_VERSION' ) ) {
    define( 'WP_RDGALLERY_VERSION', '1.0' ); // Version of plugin
}
if( !defined( 'WP_RDGALLERY_NAME_PLUGINS' ) ) {
    define( 'WP_RDGALLERY_NAME_PLUGINS', 'RD Galerías' ); // Name of plugin
}
if( !defined( 'WP_RDGALLERY_DIR' ) ) {
    define( 'WP_RDGALLERY_DIR', dirname( __FILE__ ) ); // Plugin dir
}
if( !defined( 'WP_RDGALLERY_URL' ) ) {
    define( 'WP_RDGALLERY_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if( !defined( 'WP_RDGALLERY_PLUGIN_BASENAME' ) ) {
    define( 'WP_RDGALLERY_PLUGIN_BASENAME', plugin_basename( __FILE__ ) ); // plugin base name
}
if( !defined( 'WP_RDGALLERY_POST_TYPE' ) ) {
    define( 'WP_RDGALLERY_POST_TYPE', 'rd_gallery' ); // Plugin post type
}
if( !defined( 'WP_RDGALLERY_CAT' ) ) {
    define( 'WP_RDGALLERY_CAT', 'rd_gallery_category' ); // Plugin category name
}
if( !defined( 'WP_RDGALLERY_META_PREFIX' ) ) {
    define( 'WP_RDGALLERY_META_PREFIX', '_rd_gallery_' ); // Plugin metabox prefix
}


// Plugin Functions File
require_once( WP_RDGALLERY_DIR . '/includes/rd-galeria-functions.php' );

// Plugin Post Type File
require_once( WP_RDGALLERY_DIR . '/includes/rd-galeria-post-types.php' );

// Plugin ShortCode
require_once( WP_RDGALLERY_DIR . '/includes/rd-galeria-shortcode.php' );


?>